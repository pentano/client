# Install Angular-Cli

npm install -g @angular/cli

## Start server

cd client

ng serve

Navigate to `http://localhost:4200/`
