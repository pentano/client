import { Injectable } from '@angular/core';
import { AccountsService } from '../accounts/accounts.service';
import { Observable } from 'rxjs/Observable';
import { commentsUrl, votesUrl } from '../shared/service-url';
import { JsonApiData } from '../shared/json-api-data';
import { ApiService } from '../shared/api.service';

@Injectable()
export class CommentsService {

  constructor(private api: ApiService) { }

  getConversationComments(conversationId): Observable<any>{
    return this.api.get(commentsUrl.comments.url.replace('$id', conversationId));
  }

  createComment(comment, conversationId): Observable<any>{
    const data = new JsonApiData(commentsUrl.comments.type, comment);
    return this.api.post(commentsUrl.comments.url.replace('$id', conversationId), data);
  }

  voteInComment(commentId, vote): Observable<any>{
    const data = new JsonApiData(votesUrl.votes.type, vote);
    return this.api.post(votesUrl.votes.url.replace('$id', commentId), data);
  }

  getCommentsByUser(userId): Observable<any> {
    return this.api.get(commentsUrl.authors.url.replace('$id', userId));
  }

  updateComment(comment): Observable<any> {
    const data = new JsonApiData(commentsUrl.comment.type, comment.attributes, comment.id);
    return this.api.patch(commentsUrl.comment.url.replace('$id', comment.id), data); 
  }

  deleteComment(id): Observable<any> {
    return this.api.delete(commentsUrl.comment.url.replace('$id', id)); 
  }
}

