import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { CommentsService } from '../comments.service';
import { ErrorHandleService } from '../../shared/error-handle.service';

@Component({
  selector: 'app-comment-new',
  templateUrl: './comment-new.component.html',
  styleUrls: ['./comment-new.component.css']
})
export class CommentNewComponent implements OnInit {

  @Input() conversationId;

  public comment = {};
  public errors;

  @Output() onCreated = new EventEmitter<any>();

  constructor(private commentService: CommentsService, private errorHandler: ErrorHandleService) { }

  ngOnInit() {
  }

  createComment(){
    this.commentService.createComment(this.comment, this.conversationId).subscribe(response => {
      this.onCreated.emit(response.json().data);
      this.comment = {};
    }, error => {
      this.errors = this.errorHandler.formatFormError(error.json().errors);
    });
  }
}
