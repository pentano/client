import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CommentsService } from '../comments.service';
import { ConversationsService } from '../../conversations/conversations.service';

@Component({
  selector: 'app-comment-list',
  templateUrl: './comment-list.component.html',
  styleUrls: ['./comment-list.component.css']
})
export class CommentListComponent implements OnInit {

  @Input() conversationId;
  public comments = [];

  @Output() addedVote = new EventEmitter();

  constructor(private commentService: CommentsService) { }

  ngOnInit() {
    this.commentService.getConversationComments(this.conversationId).subscribe(response => {
      this.comments = response.json().data;
    });
  }

  vote(commentId, value){
    this.commentService.voteInComment(commentId, { value }).subscribe(response => {
      this.addedVote.emit();
    });
  }

  addNewComment(comment){
    this.comments.push(comment);
  }

}
