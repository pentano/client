import { Component, OnInit } from '@angular/core';
import { CommentsService } from '../comments.service';
import { SessionService } from '../../shared/session.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-comments-user',
  templateUrl: './comments-user.component.html',
  styleUrls: ['./comments-user.component.css']
})
export class CommentsUserComponent implements OnInit {

  public comments = [];
  public showUpdatedMessage = false;

  constructor(private commentsService: CommentsService,
    private sessionService: SessionService,
    private route: Router
  ) { }

  ngOnInit() {
    const user = this.sessionService.getCurrentUser();
    if(user != null){
      this.commentsService.getCommentsByUser(user.id).subscribe(response => {
        this.comments = response.json().data;
      });
    }
    else {
      this.route.navigateByUrl('/login');
    }
  }

  updateComment(comment) {
    this.commentsService.updateComment(comment).subscribe(response => {
      this.showUpdatedMessage = true;
    });
  }

  deleteComment(comment, index) {
    this.commentsService.deleteComment(comment.id).subscribe(response => {
      this.comments.splice(index, 1);
    });
  }

}
