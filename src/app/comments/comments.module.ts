import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommentsService } from './comments.service';
import { CommentNewComponent } from './comment-new/comment-new.component';
import { CommentsUserComponent } from './comments-user/comments-user.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    SharedModule
  ],
  providers: [CommentsService],
  declarations: [CommentsUserComponent]
})
export class CommentsModule { }
