import { Component, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from '../../shared/session.service';
import { TokenService } from '../../shared/token.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  private activeSession;

  constructor(
    private sessionService: SessionService,
    private tokenService: TokenService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.sessionService.isActiveSession().subscribe(isActive => {
      this.activeSession = isActive;
    });
  }

  logOut() {
    this.tokenService.clearToken();
    this.router.navigateByUrl('/login');
  }
}
