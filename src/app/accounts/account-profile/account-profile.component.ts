import { Component, OnInit } from '@angular/core';
import { SessionService } from '../../shared/session.service';
import { Router } from '@angular/router';
import { AccountsService } from '../accounts.service';

@Component({
  selector: 'app-account-profile',
  templateUrl: './account-profile.component.html',
  styleUrls: ['./account-profile.component.css']
})
export class AccountProfileComponent implements OnInit {

  public user = {};  
  public showSuccessMessage = false;

  constructor(
    private accountsService: AccountsService,
    private sessionService: SessionService, 
    private route: Router
  ) { }

  ngOnInit() {
    this.user = this.sessionService.getCurrentUser();
    if(this.user == null) {
      this.route.navigateByUrl('/login');
    }
  }

  isFbUser() {
    return this.user['attributes'].password == '';
  }

  updateUser() {
    this.accountsService.updateUser(this.user).subscribe(response => {
      this.showSuccessMessage = true;
      this.sessionService.setCurrentUser(response.json().data);
    });
  }
}
