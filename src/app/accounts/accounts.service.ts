import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { JsonApiData } from '../shared/json-api-data';
import { usersUrl } from '../shared/service-url';
import { ApiService } from '../shared/api.service';

@Injectable()
export class AccountsService {  

  constructor(private api: ApiService) { }

  createUser(user): Observable<any> {
    const data = new JsonApiData(usersUrl.users.type, user);

    return this.api.post(usersUrl.users.url, data);
  }

  updateUser(user): any {
    const data = new JsonApiData(usersUrl.user.type, user.attributes, user.id);
    return this.api.patch(usersUrl.user.url.replace('$id', user.id), data);
  }

  signIn(user): Observable<any> {
    const formData = new FormData();
    formData.append('email', user.email);
    formData.append('password', user.password);
    return this.api.postFormData(usersUrl.signIn.url, formData);
  }

  signInWithFacebook(fbToken): Observable<any>{
    const data = new JsonApiData(
      usersUrl.signInWithFacebook.type,
      { access_token: fbToken }
    );

    return this.api.post(usersUrl.signInWithFacebook.url, data);
  }
}
