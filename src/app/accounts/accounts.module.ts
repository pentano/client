import { NgModule } from '@angular/core';
import { AccountNewComponent } from './account-new/account-new.component';
import { SharedModule } from '../shared/shared.module';
import { AccountsService } from './accounts.service';
import { AccountSigninComponent } from './account-signin/account-signin.component';
import { AccountProfileComponent } from './account-profile/account-profile.component';

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [AccountNewComponent, AccountSigninComponent, AccountProfileComponent],
  providers: []
})
export class AccountsModule { }
