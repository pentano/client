import { Component, OnInit } from '@angular/core';
import { AccountsService } from '../accounts.service';
import { ErrorHandleService } from '../../shared/error-handle.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-account-new',
  templateUrl: './account-new.component.html',
  styleUrls: ['./account-new.component.css']
})
export class AccountNewComponent implements OnInit {

  public user = {};
  public errors = null;

  constructor(private accountService: AccountsService, 
    private router: Router, 
    private errorService: ErrorHandleService) { }

  ngOnInit() {
  }

  createUser(){
    this.accountService.createUser(this.user).subscribe(response => {
      this.router.navigateByUrl('/login'); 
    }, error =>{
      this.errors = this.errorService.formatFormError(error.json().errors);
    });
  }

}
