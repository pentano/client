import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FacebookService, InitParams, LoginResponse, LoginOptions } from 'ngx-facebook';
import { AccountsService } from '../accounts.service';
import { ErrorHandleService } from '../../shared/error-handle.service';
import { TokenService } from '../../shared/token.service';

const FB_APP_ID = '1704052159613898';

@Component({
  selector: 'app-account-signin',
  templateUrl: './account-signin.component.html',
  styleUrls: ['./account-signin.component.css']
})
export class AccountSigninComponent implements OnInit {

  public user = {};
  public errors = null;

  constructor(
    private router: Router,
    private accountService: AccountsService,
    private errorService: ErrorHandleService,
    private tokenService: TokenService,
    private facebookService: FacebookService,
  ) { }

  ngOnInit() {
    let initParams: InitParams = {
      appId: FB_APP_ID,
      xfbml: true,
      version: 'v2.10',
    };

    this.facebookService.init(initParams);
  }

  signIn() {
    this.accountService
        .signIn(this.user)
        .subscribe(
          response => this.loginSuccessHandler(response),
          error => this.loginErrorHandler(error),
        );
  }

  signInWithFacebook() {

    const options: LoginOptions = {
      scope: 'public_profile,email',
      return_scopes: true,
      enable_profile_selector: true,
    };

    this.facebookService.login(options)
      .then((response: LoginResponse) => {
        const fbToken = response.authResponse.accessToken;
        console.log(fbToken);
        this.accountService
            .signInWithFacebook(fbToken)
            .subscribe(
              response => this.loginSuccessHandler(response),
              error => this.loginErrorHandler(error),
            );
      })
      .catch((error: any) => {
        console.log('fb error', error);
      });
  }

  loginSuccessHandler(response) {
    const token = response.json().data.token;
    this.errors = null;
    this.tokenService.setToken(token);
    this.router.navigateByUrl('/');
  }

  loginErrorHandler(error) {
    this.errors = this.errorService.formatFormError(error.json().errors);
  }
}
