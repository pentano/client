import { NgModule }              from '@angular/core';
import { RouterModule, Routes }  from '@angular/router';

import { AccountNewComponent } from './accounts/account-new/account-new.component'
import { AccountSigninComponent } from './accounts/account-signin/account-signin.component';
import { ConversationListComponent } from './conversations/conversation-list/conversation-list.component';
import { ConversationComponent } from './conversations/conversation/conversation.component';
import { ClusterComponent } from './conversations/cluster/cluster.component';
import { ClusterDataResolve } from './conversations/cluster/clusterData.resolve';
import { LoggedGuard } from './shared/logged.guard';
import { NotLoggedGuard } from './shared/not-logged.guard';
import { ConversationsUserComponent } from './conversations/conversations-user/conversations-user.component';
import { AccountProfileComponent } from './accounts/account-profile/account-profile.component';
import { CommentsUserComponent } from './comments/comments-user/comments-user.component';

const appRoutes: Routes = [
  {
    path: '',
    component: ConversationListComponent,
    canActivate: [
      LoggedGuard
    ],
  },
  {
    path: 'user',
    component: AccountProfileComponent,
    canActivate: [
      LoggedGuard
    ],
  },
  {
    path: 'conversation/:id',
    component: ConversationComponent,
    canActivate: [
      LoggedGuard,
    ],
    resolve: {
      cluster: ClusterDataResolve,
    },
  },
  {
    path: 'conversations/user',
    component: ConversationsUserComponent,
    canActivate: [
      LoggedGuard
    ]
  },
  {
    path: 'comments/user',
    component: CommentsUserComponent,
    canActivate: [
      LoggedGuard
    ]
  },
  {
    path: 'login',
    component: AccountSigninComponent,
    canActivate: [
      NotLoggedGuard
    ]
  },
  {
    path: 'signup',
    component: AccountNewComponent,
    canActivate: [
      NotLoggedGuard
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}