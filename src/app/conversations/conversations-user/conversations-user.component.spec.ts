import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConversationsUserComponent } from './conversations-user.component';

describe('ConversationsUserComponent', () => {
  let component: ConversationsUserComponent;
  let fixture: ComponentFixture<ConversationsUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConversationsUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConversationsUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
