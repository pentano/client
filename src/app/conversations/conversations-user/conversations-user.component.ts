import { Component, OnInit } from '@angular/core';
import { ConversationsService } from '../conversations.service';
import { SessionService } from '../../shared/session.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-conversations-user',
  templateUrl: './conversations-user.component.html',
  styleUrls: ['./conversations-user.component.css']
})
export class ConversationsUserComponent implements OnInit {

  public conversations = [];
  public showUpdatedMessage = false;

  constructor(
    private conversationsService: ConversationsService, 
    private sessionService: SessionService,
    private route: Router
  ) { }

  ngOnInit() {
    const user = this.sessionService.getCurrentUser();
    if(user != null){
      this.conversationsService.getConversationsByUser(user.id).subscribe(response => {
        this.conversations = response.json().data;
      });
    }
    else {
      this.route.navigateByUrl('/login');
    }
  }

  updateConversation(conversation){
    this.conversationsService.updateConversation(conversation).subscribe(response => {
      this.showUpdatedMessage = true;
    });
  }

  deleteConversation(conversation, index) {
    this.conversationsService.deleteConversation(conversation.id).subscribe(response => {
      this.conversations.splice(index, 1);
    });
  }
}
