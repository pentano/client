import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { ConversationsService } from '../conversations.service';

@Injectable()
export class ClusterDataResolve implements Resolve<any> {

  constructor(
    private conversationService: ConversationsService
  ) {}

  resolve(route: ActivatedRouteSnapshot) {
    return this.conversationService.getConversationCluster(route.paramMap.get('id'));
  }
}