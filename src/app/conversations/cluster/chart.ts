export class Chart {
    id: string;
    data: object;
    height: any;
    width: any;
  
    constructor (config) {
      this.id = config.id;
      this.data = config.data;
      this.height = config.height || 400;
      this.width = config.width || '100%';
    }
}
