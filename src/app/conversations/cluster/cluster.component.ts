import { Component, OnInit, NgZone, AfterViewInit, OnDestroy } from '@angular/core';
import { ConversationsService } from '../conversations.service';
import { ActivatedRoute } from '@angular/router';
import { Chart } from './chart';

declare var zingchart: any;

@Component({
  selector: 'app-cluster',
  templateUrl: './cluster.component.html',
  styleUrls: ['./cluster.component.css']
})
export class ClusterComponent implements OnInit, AfterViewInit, OnDestroy {

  public chart;

  constructor(
    private conversationService: ConversationsService,
    private route: ActivatedRoute,
    private ngZone: NgZone    
  ) {}

  ngAfterViewInit(){
    if(this.chart != null){
      this.ngZone.runOutsideAngular(() => {
        zingchart.render(this.chart);
      });
    }
  }

  ngOnInit() {
    const responseData = this.route.snapshot.data['cluster'].json();
    if(responseData != null){
      const clusters = responseData.data;
      const config = this.configChart(clusters);
      this.chart = new Chart({
        id: 'myChart',
        data: config,
        height: 500,
        width: 725
      });
    }
    else{
      this.chart = null;
    }
  }

  configChart(clusters){
    let colors = {};
    let myConfig = this.getDefaultConfig();
    for (let clusterId in clusters.pairs) {
        var clusterPoints = clusters.pairs[clusterId];
        myConfig.graphset[0].series.push({
        "values": clusterPoints,
        "data-username": clusters.users[clusterId],
        "error": {
          "line-color": "#7FC9D9",
          "line-width": 2,
          "size": 8
        },
        "errors": [
          [0, null],
          [0, null],
          [0, null]
        ],
        "text": "????????????",
        "marker": {
          "type": "circle",
          "border-width": 0,
          "size": 10,
          "background-color": this.generateColor(colors),
          "shadow": false
        },
        "palette": 0
      });
    }

    return myConfig;
  }

  getDefaultConfig(){
    return {
      "graphset": [{
        "globals": {
          "font-family": "Roboto"
        },
        "type": "scatter",
        "background-color": "#fff",
        "plotarea": {
          "margin-left": "90px",
          "margin-top": "20px"
        },
        "scaleX": {
          "guide": {
            "line-style": "solid",
            "line-width": 0, // Linha da grid
            "line-color": "#CCC"
          },
          "minor-guide": {
            "visible": 0
          },
          // "values": "-10:10:1",
          "item": {
            "font-size": "12px",
            "font-color": "#757575"
          },
          "label": {
            "padding": "10px 10px 10px 10px",
            // "text": "Estimate Age (Days)",
            "font-color": "#757575",
            "font-size": "16px"
          },
          "mirrored": false,
          "line-color": "#757575",
          "minor-ticks": 5,
          "minor-tick": {
            "placement": "outer",
            "line-color": "#757575",
            "line-width": 1,
            "size": 5
          },
          "tick": {
            "line-color": "#757575",
            "line-width": 1,
            "size": 10
          }
        },
        "scaleY": {
          // "values": "0:1:0.05",
          "item": {
            "font-size": "12px",
            "font-color": "#757575"
          },
          "label": {
            "padding": "10px 30px 10px 10px",
            // "text": "Dividends Per Share",
            "font-color": "#757575",
            "font-size": "16px"
          },
          "max-items": 20,
          "items-overlap": 1,
          "guide": {
            "line-style": "solid",
            "line-width": 0, //Linha da grid
            "line-color": "#CCC"
          },
          "minor-guide": {
            "visible": 0
          },
          "markers": [{
            "type": "line",
            "range": [0.29],
            "line-color": "#0094b3",
            "line-width": 0 // Linha do meio do gráfico
          }, {
            "type": "line",
            "range": [0.29],
            "line-color": "#F2BFBF",
            "line-width": 0 // Linha do meio do gráfico
          }],
          "min-value": "auto",
          "line-color": "#bbb",
          "minor-ticks": 5,
          "minor-tick": {
            "placement": "outer",
            "line-color": "#757575",
            "line-width": 1,
            "size": 5
          },
          "tick": {
            "line-color": "#757575",
            "line-width": 1,
            "size": 10
          }
        },
        "plot": {
          "marker": {
            "type": "circle",
            "border-width": 0,
            "background-color": "#0094b3",
            "shadow": false
          },
          "tooltip": {
            "alpha": 1,
            "shadow": true,
            "background-color": "#FFF",
            "padding": "10",
            "border-radius": 5,
            "text-align": "left",
            "placement": "horizontal",
            "color": "#000",
            "callout": true,
            "border-width": "1px",
            "border-color": "#ccc",
            "callout-position": "bottom",
            "callout-width": 16,
            "callout-height": 6,
            "height": 130,
            "font-size": 18,
            "html-mode": 1,
            "text": "<table style='width:140px;height:80px;border-collapse:collapse;'><tr><td colspan='2' style='font-size:18px !important;color:black;text-align:left;font-family:\"Roboto\";'>%data-username <br></td></tr></table>",
          }
        },
        "series": []
      }],
      "gui": {

      },
      "tween": null
    };
  }

  generateColor(colors){
    let color = '#'+Math.floor(Math.random()*16777215).toString(16);
    while(color in colors){
      color = '#'+Math.floor(Math.random()*16777215).toString(16);
    }
    colors[color] = true;
    return color;
  }

  ngOnDestroy(){
    if(this.chart != null){
      zingchart.exec(this.chart.id, 'destroy');    
    }
  }
}
