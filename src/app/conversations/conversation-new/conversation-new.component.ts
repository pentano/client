import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ConversationsService } from '../conversations.service';
import { ErrorHandleService } from '../../shared/error-handle.service';
import { AccountsService } from '../../accounts/accounts.service';

@Component({
  selector: 'app-conversation-new',
  templateUrl: './conversation-new.component.html',
  styleUrls: ['./conversation-new.component.css']
})
export class ConversationNewComponent implements OnInit {

  private conversation = {};
  private errors = null;

  @Output() onCreated = new EventEmitter();

  constructor(
    private conversationService: ConversationsService, 
    private errorService: ErrorHandleService
  ) { }

  ngOnInit() {
  }

  createConversation(){
    this.conversationService.createConversation(this.conversation).subscribe(response => {
      this.onCreated.emit(response.json().data);
      this.conversation = {};
    }, error => {
      this.errors = this.errorService.formatFormError(error.json().errors);
    });
  }

  checkIfCanCreate() {
    const titleFilled = this.conversation['title'] && this.conversation['title'] !== '';
    const descriptionFilled = this.conversation['description'] && this.conversation['description'] !== '';

    return !titleFilled || !descriptionFilled;
  }
}
