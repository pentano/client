import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConversationNewComponent } from './conversation-new.component';

describe('ConversationNewComponent', () => {
  let component: ConversationNewComponent;
  let fixture: ComponentFixture<ConversationNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConversationNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConversationNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
