import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { JsonApiData } from '../shared/json-api-data';
import { conversationsUrl } from '../shared/service-url';
import { AccountsService } from '../accounts/accounts.service';
import { ApiService } from '../shared/api.service';
import { SessionService } from '../shared/session.service';

@Injectable()
export class ConversationsService {
  
  constructor(private api: ApiService, private sessionService: SessionService) { }
  
  createConversation(conversation): Observable<any> {
    const data = new JsonApiData(conversationsUrl.conversations.type, conversation);
    
    return this.api.post(conversationsUrl.conversations.url, data);
  }
  
  getAllConversations(): Observable<any>{
    return this.api.get(conversationsUrl.conversations.url);
  }
  
  getConversationById(id): Observable<any>{
    return this.api.get(conversationsUrl.conversation.url.replace('$id', id));
  }
  
  getConversationCluster(id): Observable<any>{
    return this.api.get(conversationsUrl.clusters.url.replace('$id', id));
  }
  
  getConversationsByUser(userId) : Observable<any> {
    return this.api.get(conversationsUrl.authors.url.replace('$id', userId));
  }
  
  updateConversation(conversation: any): Observable<any> {
    const data = new JsonApiData(
      conversationsUrl.conversation.type, 
      conversation.attributes, 
      conversation.id
    );
    return this.api.patch(conversationsUrl.conversation.url.replace('$id', conversation.id), data);
  }

  deleteConversation(id: any): Observable<any> {
    return this.api.delete(conversationsUrl.conversation.url.replace('$id', id));
  }
}
