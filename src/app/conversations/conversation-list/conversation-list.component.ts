import { Component, OnInit } from '@angular/core';
import { ConversationsService } from '../conversations.service';
import { ViewChild } from '@angular/core/src/metadata/di';
import { ConversationNewComponent } from '../conversation-new/conversation-new.component';

@Component({
  selector: 'app-conversation-list',
  templateUrl: './conversation-list.component.html',
  styleUrls: ['./conversation-list.component.css']
})
export class ConversationListComponent implements OnInit {

  private conversations = [];

  constructor(private conversationService: ConversationsService) { }

  ngOnInit() {
    this.conversationService.getAllConversations().subscribe(response =>{
      this.conversations = response.json().data;
    });
  }

  addConversationOnList(conversation){
    this.conversations.push(conversation);
  }

}
