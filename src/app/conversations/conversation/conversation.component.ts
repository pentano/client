import { Component, OnInit } from '@angular/core';
import { ConversationsService } from '../conversations.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-conversation',
  templateUrl: './conversation.component.html',
  styleUrls: ['./conversation.component.css']
})
export class ConversationComponent implements OnInit {

  public conversation = null;
  public showCommentsComponent = false;
  public showSuccessMessage = false;

  constructor(
    private conversationService: ConversationsService, 
    private router: ActivatedRoute) { }
  
  ngOnInit() {
    this.router.params.subscribe(params =>{
      this.getConversation(params['id']);
    });
  }

  getConversation(id){
    this.conversationService.getConversationById(id).subscribe(response => {
      this.conversation = response.json().data;
      this.showCommentsComponent = true;
    });
  }

  showMessage() {
    this.showSuccessMessage = true;
  }
}
