import { NgModule } from '@angular/core';
import { ConversationNewComponent } from './conversation-new/conversation-new.component';
import { SharedModule } from '../shared/shared.module';
import { ConversationsService } from './conversations.service';
import { ConversationListComponent } from './conversation-list/conversation-list.component';
import { ConversationComponent } from './conversation/conversation.component';
import { ClusterComponent } from './cluster/cluster.component';
import { ClusterDataResolve } from './cluster/clusterData.resolve';
import { ConversationsUserComponent } from './conversations-user/conversations-user.component';

@NgModule({
  imports: [
    SharedModule
  ],
  providers: [
    ConversationsService,
    ClusterDataResolve,
  ],
  declarations: [
    ConversationNewComponent,
    ConversationListComponent,
    ConversationComponent,
    ClusterComponent,
    ConversationsUserComponent,
  ]
})
export class ConversationsModule { }
