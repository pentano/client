import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { SessionService } from './session.service';

@Injectable()
export class LoggedGuard implements CanActivate {

  constructor(
    private session: SessionService,
    private router: Router
  ) { }

  canActivate(): Promise<boolean> {
    return this.session.isLoggedIn().then(
      response => {
        this.session.setActiveSession(true, response.json().data);
        return true;
      },
      error => {
        this.session.setActiveSession(false);        
        this.router.navigateByUrl('/login');
        return false;
      }
    );
  }
}
