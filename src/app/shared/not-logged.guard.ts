import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { SessionService } from './session.service';

@Injectable()
export class NotLoggedGuard implements CanActivate {
  
  constructor(
    private session: SessionService,
    private router: Router
  ) { }
  
  canActivate(): Promise<boolean> {
    return this.session.isLoggedIn().then(
      response => {
        this.session.setActiveSession(true, response.json().data);
        this.router.navigateByUrl('/');
        return false;
      },
      error => {
        this.session.setActiveSession(false);
        return true;
      }
    );
  }
}
