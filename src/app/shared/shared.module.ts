import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ErrorHandleService } from './error-handle.service';
import { AccountsService } from '../accounts/accounts.service';
import { RouterModule } from '@angular/router';
import { CommentListComponent } from '../comments/comment-list/comment-list.component';
import { ApiService } from './api.service';
import { TokenService } from './token.service';
import { SessionService } from './session.service';
import { LoggedGuard } from './logged.guard';
import { NavbarComponent } from '../menus/navbar/navbar.component';
import { NotLoggedGuard } from './not-logged.guard';
import { CommentNewComponent } from '../comments/comment-new/comment-new.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule
  ],
  exports: [
    CommonModule,
    FormsModule,
    RouterModule,
    CommentListComponent,
    NavbarComponent,
    CommentNewComponent
  ],
  providers: [
    ErrorHandleService,
    AccountsService,
    ApiService,
    TokenService,
    SessionService,
    LoggedGuard,
    NotLoggedGuard
  ],
  declarations: [CommentListComponent, CommentNewComponent, NavbarComponent],
})
export class SharedModule { }
