import { Injectable } from '@angular/core';

@Injectable()
export class TokenService {

  constructor() { }

  getToken(){
    return localStorage.getItem('token');
  }

  setToken(token){
    localStorage.setItem('token', token);
  }

  clearToken(){
    this.setToken('');
  }
}
