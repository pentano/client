export class JsonApiData {

    private id;
    private type;
    private attributes;
    private relationships;

    constructor(type, attributes, id?, relationships?){
        this.type = type;
        this.attributes = attributes;
        this.id = id;
        if(relationships){
            this.relationships = relationships;
        }
    }
}
