import { Injectable } from '@angular/core';

@Injectable()
export class ErrorHandleService {

  constructor() { }

  formatFormError(error){
    let errors = [];
    const attrs = Object.keys(error);
    for(let i=0; i < attrs.length; i++){
      let attrName = attrs[i];
      error[attrName].forEach(message => {
        errors.push({field: attrName, message: message});
      });
    }
    return errors;
  }
}
