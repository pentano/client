import { environment } from '../../environments/environment';

export const usersUrl = {
    users: {
        type: 'users',
        url: '/users/'
    },
    user: {
        type: 'usersDetails',
        url: '/users/$id'
    },
    signIn: {
        url: '/login/'
    },
    signInWithFacebook: {
        url: '/login/facebook/',
        type: 'login_with_facebook',
    },
    current: {
        url: '/users/current/'
    },
}

export const conversationsUrl = {
    conversations: {
        type: 'conversations',
        url: '/conversations/'
    },

    conversation: {
        type: 'conversationDetails',
        url: '/conversations/$id'
    },

    clusters: {
        type: '',
        url: '/conversations/$id/clusters'
    },

    authors: {
        type: '',
        url: '/conversations/authors/$id'
    }
}

export const commentsUrl = {
    comments: {
        type: 'comments',
        url: '/conversations/$id/comments'
    },

    comment: {
        type: 'commentDetails',
        url: '/conversations/comments/$id'
    },

    authors: {
        type: '',
        url: '/conversations/authors/$id/comments'
    }
}

export const votesUrl = {
    votes: {
        type: 'votes',
        url: '/conversations/comments/$id/votes'
    }
}