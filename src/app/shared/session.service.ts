import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { ApiService } from './api.service';
import { usersUrl } from '../shared/service-url';
import 'rxjs/add/operator/toPromise';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class SessionService {

  private activeSession = new Subject();
  private currentUser = null;

  constructor(private api: ApiService, private router: Router) {
    this.activeSession.next(false);
  }

  isActiveSession(){
    return this.activeSession.asObservable();
  }

  setActiveSession(activeSession, currentUser?){
    this.activeSession.next(activeSession);
    this.setCurrentUser(currentUser);
  };

  setCurrentUser(currentUser) {
    this.currentUser = currentUser;
  }

  getCurrentUser() {
    return this.currentUser;
  } 
  
  isLoggedIn(): Promise<any> {
    return this.api.get(usersUrl.current.url).toPromise();
  }
}
