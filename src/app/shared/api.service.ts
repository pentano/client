import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Headers, Response } from '@angular/http';
import { TokenService } from './token.service';
import { environment } from '../../environments/environment';

@Injectable()
export class ApiService {

  private headers = new Headers();

  constructor(private http: Http, private tokenService: TokenService) {
    this.addHeaders();
  }

  get(url): Observable<Response>{
    this.addAuthorizationHeader();
    return this.http.get(
      `${environment.apiUrl}${url}`,
      { headers: this.headers },
    );
  }

  post(url, data): Observable<Response>{
    this.addAuthorizationHeader();
    return this.http.post(
      `${environment.apiUrl}${url}`,
      { data },
      { headers: this.headers },
    );
  }

  put(url, data): Observable<Response>{
    this.addAuthorizationHeader();
    return this.http.put(
      `${environment.apiUrl}${url}`,
      { data },
      { headers: this.headers },
    );
  }

  patch(url, data): Observable<Response>{
    this.addAuthorizationHeader();
    return this.http.patch(
      `${environment.apiUrl}${url}`,
      { data },
      { headers: this.headers },
    );
  }

  delete(url): Observable<Response>{
    this.addAuthorizationHeader();
    return this.http.delete(
      `${environment.apiUrl}${url}`,
      { headers: this.headers },
    );
  }

  postFormData(url, formData): Observable<Response>{
    this.addAuthorizationHeader();
    return this.http.post(
      `${environment.apiUrl}${url}`,
      formData
    );
  }

  private addHeaders(){
    this.headers.set('Content-Type', 'application/vnd.api+json');
    this.addAuthorizationHeader();
  }

  private addAuthorizationHeader(){
    const token = this.tokenService.getToken();
    if (token) {
      this.headers.set('Authorization', `JWT ${token}`.trim());
    }else{
      this.headers.delete('Authorization');
    }
  }
}
